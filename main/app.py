import datetime
from typing import List

from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///hw.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    from .models import Client, ClientParking, Parking

    @app.before_first_request
    def before_request_func():
        db.create_all()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/clients/", methods=["GET"])
    def get_clients_handler():
        """
        Получить всех клиентов
        """
        clients: List[Client] = db.session.query(Client).all()
        client_list = [u.to_json() for u in clients]
        return jsonify(client_list), 200

    @app.route("/clients/<int:client_id>/", methods=["GET"])
    def get_client_handler(client_id: int):
        """
        Получить клиента по id
        """
        client: Client = db.session.query(Client).get(client_id)
        return jsonify(client.to_json()), 200

    @app.route("/clients/", methods=["POST"])
    def create_client_handler():
        """
        Создание нового клиента
        """
        name = request.form.get("name", type=str)
        surname = request.form.get("surname", type=str)
        credit_card = request.form.get("credit_card", type=str)
        car_number = request.form.get("car_number", type=str)

        new_client = Client(
            name=name, surname=surname, credit_card=credit_card, car_number=car_number
        )

        db.session.add(new_client)
        db.session.commit()

        return "", 201

    @app.route("/parkings/", methods=["POST"])
    def create_parkings_handler():
        """
        Создание новой парковочной зоны
        """
        address = request.form.get("address", type=str)
        opened = request.form.get("opened", type=bool)
        count_places = request.form.get("count_places", type=int)
        count_available_places = request.form.get("count_available_places", type=int)

        new_parking = Parking(
            address=address,
            opened=opened,
            count_places=count_places,
            count_available_places=count_available_places,
        )

        db.session.add(new_parking)
        db.session.commit()
        return "", 201

    @app.route("/client_parkings/", methods=["POST"])
    def create_client_parkings_handler():
        """
        Заезд на парковку
        """
        client_id = request.form.get("client_id", type=int)
        parking_id = request.form.get("parking_id", type=int)

        parking = db.session.query(Parking).get(parking_id)

        if parking.opened:
            new_client_parkings = ClientParking(
                client_id=client_id,
                parking_id=parking_id,
                time_in=f"{datetime.datetime.now()}",
            )
            parking.count_available_places -= 1

            db.session.add(new_client_parkings)

        db.session.commit()
        return "", 201

    @app.route("/client_parkings/", methods=["DELETE"])
    def delete_client_parkings_handler():
        """
        Выезд с парковки
        """
        client_id = request.form.get("client_id", type=int)
        parking_id = request.form.get("parking_id", type=int)

        client_parking = db.session.query(ClientParking).get(client_id)
        parking = db.session.query(Parking).get(parking_id)

        if client_parking:
            client_parking.time_out = f"{datetime.datetime.now()}"
            parking.count_available_places += 1

        db.session.commit()
        return "", 201

    return app
