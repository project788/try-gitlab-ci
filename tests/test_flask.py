import pytest


def test_create_client(client) -> None:
    client_data = {
        "name": "Test_name",
        "surname": "Test_surname",
        "credit_card": "2222222222222222",
        "car_number": "A111AA"
    }
    resp = client.post("/clients/", data=client_data)

    assert resp.status_code == 201


def test_create_parkings(client) -> None:
    parkings_data = {
        "address": "address",
        "opened": True,
        "count_places": 5,
        "count_available_places": 9
    }
    resp = client.post("/parkings/", data=parkings_data)

    assert resp.status_code == 201


def test_app_config(app):
    assert not app.config['DEBUG']
    assert app.config['TESTING']
    assert app.config['SQLALCHEMY_DATABASE_URI'] == "sqlite://"


@pytest.mark.parametrize("route", ["/clients/", "/clients/1/"])
def test_route_status(client, route):
    rv = client.get(route)
    assert rv.status_code == 200


@pytest.mark.parking
def test_create_client_parkings(client):
    _data = {
        "client_id": 1,
        "parking_id": 1,
    }
    resp = client.post("/client_parkings/", data=_data)

    assert resp.status_code == 201


@pytest.mark.parking
def test_delete_client_parkings(client):
    _data = {
        "client_id": 1,
        "parking_id": 1,
    }
    resp = client.delete("/client_parkings/", data=_data)

    assert resp.status_code == 201
