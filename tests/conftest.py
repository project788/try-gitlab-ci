import pytest

try:
    from main.models import Client, Parking, ClientParking
    from main.app import create_app, db as _db
except ImportError:
    from ..main.models import Client, Parking, ClientParking
    from ..main.app import create_app, db as _db


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all()
        parking = Parking(
            address="Проспект Канадского",
            opened=True,
            count_places=10,
            count_available_places=10
        )
        client = Client(
            name="Иван",
            surname='Иванов',
            credit_card='1111111111111111',
            car_number='A777AA'
        )
        _db.session.add(parking)
        _db.session.add(client)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
